<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Products;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//seeder product

        // Product::create(
        // 	['category_id'=> ''],
        // 	['name'=> 'Category 1'],
        // 	['unit_price'=> '2000'],
        // );

        //get random category seeder

    		// $category= Category::first();

      //   //Create insert product
    		// Products::create(
      //   	[ 'category_id'=>$category->id,
      //   		'name' => 'category 1',
      //   		'unit_price' => 1000

      //   	]
        	
      //   );

    	//factory product
 		factory(Products::class,3)->create([
 				'category_id' =>Category::first(), //ketika ada foreign  key
 			]);


    }
}
