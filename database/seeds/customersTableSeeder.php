<?php

use Illuminate\Database\Seeder;
use App\Customers;

class customersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Customers::class,3)->create();
    }
}
