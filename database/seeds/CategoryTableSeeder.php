<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        factory(Category::class,3)->create([
                'product_count' =>'0' //ketika ada foreign  key
            ]); //factory category
        // Category::create(['name'=> 'Category 1']); //untuk manual seeder

    }
}
