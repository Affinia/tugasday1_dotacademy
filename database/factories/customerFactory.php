<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Customers;

$factory->define(Customers::class, function (Faker $faker) {
    return [
        'email'=> $faker->email,
        'first_name'=> $faker->word,
        'last_name'=> $faker->word,
        'address'=> $faker->address,
        'phone_number'=> $faker->tollFreePhoneNumber,
        'password'=> $faker->password 

    ];
});
