<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Products;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
    	
        'name'=> $faker->word,
        'unit_price'=> $faker->buildingNumber
    ];
});
