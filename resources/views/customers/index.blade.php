@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customers
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tambah Customers</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="/customers/store" method="post">
          
            @csrf 
            <!-- {{ csrf_field() }} -->
            <div class="box-body">
              @if (Session::has('massage'))
              <div class="alert alert-success">{{Session::get('massage')}}</div>
              @endif

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              <div class="form-group">
                <label for="exampleInputEmail1">Nama Depan</label>
                <input type="text" name="first_name" class="form-control" placeholder="Masukkan nama depan">

                <label for="exampleInputEmail1">Nama Belakang</label>
                <input type="text" name="last_name" class="form-control" placeholder="Masukkan nama belakang">

                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" class="form-control" placeholder="Masukkan Email (email@email.com)">

                <label for="exampleInputEmail1">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Masukkan password">

                <label for="exampleInputEmail1">Alamat</label>
                <input type="text" name="address" class="form-control" placeholder="Masukkan Alamat">

                <label for="exampleInputEmail1">Nomor HP</label>
                <input type="number" name="phone_number" class="form-control" placeholder="Masukkan Nomor HP">
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Daftar Customers</h3>
            <form action="/customers" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th>No</th>
                <th>Nama Depan</th>
                <th>Nama Belakang</th>
                <th>Email</th>
                <th>Address</th>
                <th>phone number</th>
                <th>password</th>
                <th>Aksi</th>
              </tr>
              <?php $no=0;?>
              @foreach($dataCustomers as $cust)
              <?php $no++; ?>
              <tr>
                <td>{{ ++$number }}</td>
                <td>{{ $cust->first_name }}</td>
                <td>{{ $cust->last_name }}</td>
                <td>{{ $cust->email }}</td>
                <td>{{ $cust->address }}</td>
                <td>{{ $cust->phone_number }}</td>
                <td>{{ $cust->password }}</td>
                <td>
                  <a href="/customers/{{$cust->id}}" class="btn btn-success">Detail</a>
                  <a href="/customers/edit/{{$cust->id}}" class="btn btn-primary">Edit</a>
                  <a href="/customers/{{$cust->id}}/delete" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </table>
            <div class="text-center">
              {!! $dataCustomers->appends(request()->all())->links() !!}
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')