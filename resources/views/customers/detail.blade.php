@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customers
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
       <!--  -->
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Detail Customers</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <td>id</td>
                <td>:</td>
                <td>{{ $dataCustomers->id }}</td>
              </tr>
              <tr>
                <td>nama</td>
                <td>:</td>
                <td>{{ $dataCustomers -> first_name }}</td>
              </tr>
              <tr>
                <td>nama belakang</td>
                <td>:</td>
                <td>{{ $dataCustomers -> last_name }}</td>
              </tr>
               <tr>
                <td>address</td>
                <td>:</td>
                <td>{{ $dataCustomers -> address }}</td>
              </tr>
               <tr>
                <td>phone number</td>
                <td>:</td>
                <td>{{ $dataCustomers -> phone_number }}</td>
              </tr>
              <tr>
                <td>email</td>
                <td>:</td>
                <td>{{ $dataCustomers -> email }}</td>
              </tr>
              <tr>
                <td>password</td>
                <td>:</td>
                <td>{{ $dataCustomers -> password }}</td>
              </tr>
            </table>
            <br>
            <a href="/customers" class="btn btn-primary">Back</a>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')