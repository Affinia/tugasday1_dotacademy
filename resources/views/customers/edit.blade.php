@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customers
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
       <!--  -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Customers</h3>
          </div>
          <!-- /.box-header -->
          @if(Session::has('message'))
           <h4><strong>{{ Session::get('message')}}</strong>
           @endif
          @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
          <form role="form" action="/customers/{{ $dataCustomers->id }}/update" method="post">
           @csrf
          <div class="box-body">
            <table class="table table-bordered">
            <tr>
                <td>id</td>
                <td>:</td>
                <td><input type="text" required="required" name="id" value="{{ $dataCustomers->id }}"></td>
              </tr>
              <tr>
                <td>nama depan</td>
                <td>:</td>
                <td><input type="text" required="required" name="first_name" value="{{ $dataCustomers->first_name }}"></td>
              </tr>
              <tr>
                <td>nama belakang</td>
                <td>:</td>
                <td><input type="text" required="required" name="last_name" value="{{ $dataCustomers->last_name }}"></td>
              </tr>
           <tr>
                <td>email</td>
                <td>:</td>
                <td><input type="text" required="required" name="email" value="{{ $dataCustomers->email }}"></td>
              </tr>
              <tr>
                <td>password</td>
                <td>:</td>
                <td><input type="text" required="required" name="password" value="{{ $dataCustomers->password }}"></td>
              </tr>
              <tr>
                <td>alamat</td>
                <td>:</td>
                <td><input type="text" required="required" name="address" value="{{ $dataCustomers->address }}"></td>
              </tr>
              <tr>
                <td>phone number</td>
                <td>:</td>
                <td><input type="text" required="required" name="phone_number" value="{{ $dataCustomers->phone_number }}"></td>
              </tr>
            </table>
            <br>
                <input type="submit" class="btn btn-warning" value="Update">
                <a href="/customers" class="btn btn-warning">Back</a>
              
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')