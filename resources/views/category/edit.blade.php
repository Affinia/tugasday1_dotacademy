@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
       <!--  -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Category</h3>
          </div>
          <!-- /.box-header -->
          @if(Session::has('message'))
           <h4><strong>{{ Session::get('message')}}</strong>
           @endif
          @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
          <form role="form" action="/category/{{ $dataCategory->id }}/update" method="post">
           @csrf
          <div class="box-body">
            <table class="table table-bordered">
            <tr>
                <td>id</td>
                <td>:</td>
                <td><input type="text" required="required" name="id" value="{{ $dataCategory->id }}"></td>
              </tr>
              <tr>
                <td>nama</td>
                <td>:</td>
                <td><input type="text" required="required" name="name" value="{{ $dataCategory->name }}"></td>
              </tr>
              
            </table>
            <br>
                <input type="submit" class="btn btn-warning" value="Update">
                <a href="/category" class="btn btn-warning">Back</a>
              
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')