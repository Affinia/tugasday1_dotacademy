@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detail Order
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Form Produk</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{ route('order.add', $detailId) }}" method="post">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Produk</label>
                <select name="product_id" class="form-control">
                  <option value="">Pilih</option>
                  @foreach($dataProduct as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endforeach
                </select>
                <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah</label>
                    <input type="text" name="quantity" class="form-control" placeholder="Masukkan email">
                </div>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>

    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br/>
    @endif
    
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Contoh Tabel</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th>No</th>
                <th>Kode Order</th>
                <th>Nama Produk</th>
                <th>Unit Price</th>
                <th>Jumlah Barang</th>
                <th>Harga Total</th>
                
              </tr>
              <?php $no = 1 ?>
              @foreach($dataOrderDetail as $item)
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $item->order_id }}</td>
                  <td>{{ $item->product->name }}</td>
                  <td>{{ $item->product->unit_price }}</td>
                  <td>{{ $item->quantity }}</td>
                  <td>Rp{{ number_format($item->price, 2) }}</td>
                </tr>
                <?php $no++ ?>
              @endforeach
              <tr>
                <td colspan="5" class="text-center">Total</td>
                <td>Rp{!! number_format($total->total,2) !!}</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('base.footer')