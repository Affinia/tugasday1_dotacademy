@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Order
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Customer Order</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="/order/store" method="POST">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Customer</label>
                <select name="customer_id" class="form-control">
                  <option value="">Pilih nama customer</option>
                  @foreach($dataCustomer as $item)
                    <option value="{{ $item->id }}">{{ $item->first_name }} {{ $item->last_name }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>

    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br/>
    @endif
    
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Contoh Tabel</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th>Kode Order</th>
                <th>Nama Customer</th>
                <th>Total</th>
                <th>Aksi</th>
              </tr>
              @foreach($dataOrder as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->customers->first_name }} {{ $item->customers->last_name }}</td>
                  <td>{{ $item->total }}</td>
                  <td>
                  <a href="/order/detail/{{ $item->id }}" class="btn btn-primary btn-edit"><span class="fa fa-edit"></span>show detail</a>
                  <a href="/order/{{ $item->id }}/delete" class="btn btn-warning btn-edit"><span class="fa fa-trash"></span>delete</a>
                    
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
        </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('base.footer')
