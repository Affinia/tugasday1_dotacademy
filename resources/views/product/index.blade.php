@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tambah Product</h3>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          @if(Session::has('message'))
           <h4><strong>{{ Session::get('message')}}</strong>
           @endif

           @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
          <form role="form" action="/product/store" method="post" enctype="multipart/form-data">
           

           @csrf

               
            <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" class="form-control" placeholder="Masukkan nama kategori produk" name="name">
                </div>
              </div>
              <div class="box-body">
              <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="category_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($dataCategory as $category)
                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                 </div>
                </div>

            <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="text" class="form-control" placeholder="Masukkan nama kategori produk" name="unit_price">
                </div>
              </div>

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Foto</label>
                  <input type="file" name="image" class="form-control">
                </div>
              </div>
              
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Daftar Product</h3>
          </div>
        <!-- form kolom search-->
          <form action="/product" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
          </form>

          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th>No</th>
                <th>Nama Product</th>
                <th>Category</th>
                <th>Harga</th>
                <th>Foto</th>
                <th>Action</th>
              </tr>              
                
                  @foreach($dataProduct as $item)

              <tr>
                <td>{{++$number}}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->category->name }}</td>
                <td>{{ $item->unit_price }}</td>
                <td><img src="/images/{{ $item->image }}" style="width: 50px; height: 40px"></td>
                <td>
                  <a href="/product/edit/{{$item->id}}" class="btn btn-primary btn-edit"><span class="fa fa-edit"></span>Edit</a>
                  <a href="/product/{{$item->id}}/delete" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span>delete</a>
                  <a href="/product/{{$item->id}}" class="btn btn-primary btn-edit"><span class="fa fa-edit"></span>Detail</a>
                </td>
              </tr>
              
           @endforeach
            </table>
          <!--pagination-->
            <div class="text-center">
              {!! $dataProduct->appends(request()->all())->links() !!}
          </div>
          </div>
          

        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')