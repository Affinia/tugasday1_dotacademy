@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
       <!--  -->
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Detail Product</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <td>id</td>
                <td>:</td>
                <td>{{ $dataProduct->id }}</td>
              </tr>
              <tr>
                <td>nama</td>
                <td>:</td>
                <td>{{ $dataProduct->name }}</td>
              </tr>
              <tr>
                <td>category</td>
                <td>:</td>
                <td>{{ $dataProduct->category->name }}</td>
              </tr>
              <tr>
                <td>harga</td>
                <td>:</td>
                <td>{{ $dataProduct->unit_price }}</td>
              </tr>
              <tr>
                 <td>foto</td>
                <td>:</td>
                <td><img src="/images/{{ $dataProduct->image }}" style="width: 50px; height: 40px"></td>
              </tr>

           
            </table>
            <br>
            <a href="/product" class="btn btn-primary">Back</a>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')