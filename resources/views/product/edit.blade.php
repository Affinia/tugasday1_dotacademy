@include('base.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
       <!--  -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Product</h3>
          </div>
          <!-- /.box-header -->
          @if(Session::has('message'))
           <h4><strong>{{ Session::get('message')}}</strong>
           @endif
          @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
          <form role="form" action="/product/{{ $dataProduct->id }}/update" method="post" enctype="multipart/form-data">
           @csrf
          <div class="box-body">
            <table class="table table-bordered">
            <tr>
                <td>id</td>
                <td>:</td>
                <td><input type="text" required="required" name="id" value="{{ $dataProduct->id }}"></td>
              </tr>
              <tr>
                <td>nama</td>
                <td>:</td>
                <td><input type="text" required="required" name="name" value="{{ $dataProduct->name }}"></td>
              </tr>
              <tr>
                <td>Category</td>
                <td>:</td>
                <td>
                  <select class="form-control" name="category_id" id="category_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($dataCategory as $category)
                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                 </td>

              </tr>
           <tr>
                <td>Harga</td>
                <td>:</td>
                <td><input type="text" required="required" name="unit_price" value="{{ $dataProduct->unit_price }}"></td>
              </tr>

            <tr>
                <td>Foto Product</td>
                <td>:</td>
                <td><img src="/images/{{ $dataProduct->image }}" style="width: 50px; height: 40px">
                <input type="file" name="image">
                </td>
              </tr>

            </table>

            <br>
                <input type="submit" class="btn btn-warning" value="Update">
                <a href="/product" class="btn btn-warning">Back</a>
              
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @include('base.footer')

  <script type="text/javascript">
    $('#category_id').val('{{$dataProduct->category_id}}');
  </script> <!--select kategori muncul di edit-->