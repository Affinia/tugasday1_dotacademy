<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
route login
*/
Route::get("/login", "LoginController@showLogin")->name("login"); // show login form
Route::post("/login", "LoginController@doLogin")->name("login.doLogin");
/*
route logout
*/
Route::get("/logout", "LoginController@logout")->name("login.logout");

/*
route untuk sebelu loginn ditambahkan
*/
Route::middleware("auth")->group(function(){

Route::get('/', function () {
    return view('testing',['data' => []]);
});

Route::get('/nama', function () {
    return "Affinia";
}); //statis

Route::get('/hello/{nama}', function ($nama) {
    return "hello $nama ";
}); //dinamis

Route::get('/hello/{nama}/{mobil}', function ($nama,$mobil) {
    return "hello $nama $mobil";  

});

Route::get('/home', 'cobaController@home');
Route::get('/home/{nama}', 'cobaController@homeNama'); //masuk ke controller
//route category
Route::group(['prefix' => 'category'],function(){
Route::get('/', 'CategoryController@index');
Route::get('/tambah','CategoryController@tambah');
Route::get('/{id}','CategoryController@detail');
Route::get('/edit/{id}','CategoryController@edit');
Route::post('/store','CategoryController@store');
Route::post('/{id}/update','CategoryController@update');
Route::get('/{id}/delete','CategoryController@delete');
});

//route product
Route::group(['prefix' => 'product'],function(){
Route::get('/', 'ProductsController@index');
Route::get('/tambah','ProductsController@tambah');
Route::get('/{id}','ProductsController@detail');
Route::get('/edit/{id}','ProductsController@edit');
Route::post('/store','ProductsController@store');
Route::post('/{id}/update','ProductsController@update');
Route::get('/{id}/delete','ProductsController@delete');
});
//route customers
Route::group(['prefix' => 'customers'],function(){
Route::get('/', 'CustomersController@index');
Route::get('/{id}','CustomersController@detail');
Route::post('/store','CustomersController@store');
Route::get('/edit/{id}','CustomersController@edit');
Route::post('/{id}/update','CustomersController@update');
Route::get('/{id}/delete','CustomersController@delete');
Route::get('/create','CustomersController@create');
});
//route orders
Route::group(['prefix' => 'order'],function(){
Route::get('/', 'OrderController@index');
Route::post('/store', 'OrderController@store');
Route::get('/{id}/delete', 'OrderController@destroy');
Route::get('/detail/{id}', 'OrderController@detail');
Route::post('/detail/{id}/add', 'OrderController@addProduct')->name('order.add');
});

});

