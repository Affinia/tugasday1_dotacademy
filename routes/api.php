<?php

use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Category;
use App\Products;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::prefix('v1')->group(function()
// {
// 	Route::prefix('belajar')->group(function(){
// 	Route::get('cobaget','API\V1\CobaController@method_get');
// 	Route::post('cobapost','API\V1\CobaController@method_post');
// 	Route::post('cobatry','API\V1\CobaController@trycatch');
// 	});

// 	Route::prefix('/')->group(function(){
// 	Route::get('category','API\V1\CategoryController@index');
// 	Route::get('category/{id}','API\V1\CategoryController@detail');
// 	Route::post('category/add','API\V1\CategoryController@store');
// 	});

// 	Route::prefix('/')->group(function(){
// 	Route::get('customers','API\V1\CustomersController@index');
// 	Route::get('customers/{id}','API\V1\CustomersController@detail');
// 	Route::post('customers/add','API\V1\CustomersController@store');
// 	});
// });

	
	Route::post('/register', 'API\Auth\RegisterController@register');
	Route::post('/login', 'API\Auth\LoginController@login');


	Route::group(['middleware' => 'auth:api'], function () {
    // user detail
    Route::get('/user/detail', 'API\UserController@detail');
    Route::apiResource("category","API\V1\CategoryController");
	Route::apiResource("customers","API\V1\CustomersController");
	Route::apiResource("products","API\V1\ProductsController");
	Route::apiResource("category","API\V1\CategoryController");
	Route::apiResource("order","API\V1\OrderController");
	Route::apiResource("order_detail","API\V1\OrderDetailController");
	});

	// Route::get('/products', function() {
 //    return apiResponseBuilder(200, ProductResource::collection(products::all()));
	// });

	// Route::get('/products/{id}', function($id) {
 //    return apiResponseBuilder(200,new ProductResource(products::find($id)));
	// });

	// Route::get('/products', function() {
 //    return apiResponseBuilder(200, new ProductCollection(products::all())); 
 //    ;
	// });
