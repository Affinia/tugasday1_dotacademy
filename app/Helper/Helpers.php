<?php 
if (! function_exists('apiResponseBuilder')) {
      function apiResponseBuilder($code,$data)
    {
    	$response['status'] = $code;
        $response['data'] = $data;
    	return response()->json($response,$code);
	}

	function apiResponseValidationFails($message = null, $errors = null, $status_code = 422) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => [
            'errors' => $errors
        ]
    ], $status_code);
	}

	function apiResponseSuccess($message = null, $data = null, $status_code = 200) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => $data
    ], $status_code);
	}

	function apiResponseErrors($message = null, $data = null, $status_code = 401) {
    return response()->json([
        'message' => $message,
        'status_code' => $status_code,
        'data' => $data
    ], $status_code);
	}
}