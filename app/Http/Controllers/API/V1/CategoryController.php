<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Category;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
        	$dataCategory = Category::query();
        	if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCategory->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
        	}
        	$pagination = 5;
	        $dataCategory= $dataCategory->orderBy('created_at','desc') //sorting
	                                    ->paginate(5);

	        // Handle perpindahan page
	        $number = 0; // Default page

	        if (request()->has('page') && request()->get('page') >= 1) {
	            $number += (request()->get('page') - 1) * $pagination;
	        }
			return apiResponseBuilder(200,$dataCategory);
        } catch (Exception $e) {
        	$response=$e->getMessage();
			$code=500;	
			return apiResponseBuilder(500,$e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
        	$this->validate($request,[
    		'name' => 'required',
    		
    		]);
	        $dataCategory = new Category;

	    	$dataCategory->name = $request->name;
	        $dataCategory->product_count = 0;
	    	
	    	$dataCategory->save();
	    	$response=$dataCategory;
			$code=200;	
        } catch (Exception $e) {
        	if($e instanceof ValidationException){
	        	$response=$e->errors();
				$code=400;
        	}else{
        		$response=$e->getMessage();
				$code=500;
        	}
        					
        }
        return apiResponseBuilder($code,$response);	 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        try {
        	
        	$dataCategory = Category::with("products")->findOrFail($id);
        	return apiResponseBuilder(200,$dataCategory);

        } catch (Exception $e) {
        	if($e instanceof ModelNotFoundException){        	
				$code=404;	
				return apiResponseBuilder(404,"Data Tidak Ada");
        	}else{
	        	$response=$e->getMessage();
				$code=500;	
			return apiResponseBuilder(500,$e->getMessage());
			}
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
        	$this->validate($request,[
    		'name' => 'required',
    		
    		]);
        	$dataCategory = Category::find($id);
	    	$dataCategory->name = $request->name;
	    	$dataCategory->save();
	    	return apiResponseBuilder(200,$dataCategory);
        } catch (Exception $e) {
        	
        	if($e instanceof ValidationException){
	        	$response=$e->errors();
				$code=400;
        	}else{
        		$response=$e->getMessage();
				$code=500;
        	}
        	return apiResponseBuilder($code,$response);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        	$dataCategory = Category::find($id);

    		$dataCategory->delete();
    		return apiResponseBuilder(200,$dataCategory);
        } catch (Exception $e) {
        	$response=$e->getMessage();
			$code=500;	
			return apiResponseBuilder(500,$e->getMessage());
	
        }
    }
}
