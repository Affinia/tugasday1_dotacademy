<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Exception;
use App\OrderDetail;
use App\Customers;
use App\Products;
use App\Orders;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            //menampilkan data dan menampilkan data
        $dataCustomer = Customers::all();
        $dataOrder = Orders::all();
        $code=200;
        $response=$dataOrder;
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;  
            
        }
        return apiResponseBuilder($code,$response);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
        $request->validate([
            'customer_id'   => 'required',
        ]);

        $dataOrder = new Orders;
        $dataOrder->customer_id = $request->customer_id;
        
        $dataOrder->total       = 0;
        $dataOrder->save();
        $response=$dataOrder;
        $code=200; 
        } catch (Exception $e) {
            if($e instanceof ValidationException){
                $response=$e->errors();
                $code=400;
            }else{
                $response=$e->getMessage();
                $code=500;
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {$data = Orders::findOrFail($id);
           $code = 200;
           $response = $data;        
       } catch (Exception $e) {            
        if ($e instanceof ModelNotFoundException)
         {
               $code = 404;
               $response = "not found data";
           }else{
               $code = 500;
               $response = $e->getMessage();
           }
       }
       return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dataOrder = orders::withTrashed()->find($id);

        if (!$dataOrder->trashed()) {

            $dataOrder->delete();
        }else{
            $dataOrder->forceDelete();
        }
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;  
            return apiResponseBuilder(500,$e->getMessage());
        }
    }


}
