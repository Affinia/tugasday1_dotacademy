<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\OrderDetail;
use App\Customers;
use App\Products;
use App\Orders;
use Exception;
class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
            'order_id'      => 'required',
            'product_id'    => 'required',
            'quantity'      => 'required',
           
        ]);

        // get data Order detail by product id
        $checkProduct = OrderDetail::where('product_id', $request->product_id)->where('order_id', $request->order_id)->first();
        //mengammbil data harga product
        $dataProduct= Products::where('id', $request->product_id)->first();        

        //cek jika produk sudah ada/belum
        if ($checkProduct) {            
            $checkProduct->order_id = $checkProduct->order_id;
            $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
            $checkProduct->price = $checkProduct->price + ($request->quantity * $dataProduct->unit_price);            
            $checkProduct->save();
        } else { //jika produk tidak ada maka menambahkan              
            $dataOrderDetail = new OrderDetail;
            $dataOrderDetail->order_id      = $request->order_id;
            $dataOrderDetail->product_id    = $request->product_id;
            $dataOrderDetail->quantity      = $request->quantity;
            $dataOrderDetail->price         = $request->quantity * $dataProduct->unit_price;
            $dataOrderDetail->save();
        }

        //ambil price  tabel simple order
        $getOrder = OrderDetail::where('order_id', $request->order_id)->get();
        //menjumlah semua price di order
        $total = $getOrder->sum('price');

        //insert data total order
        $dataOrder=Orders::find($request->order_id);
        $dataOrder->total=$total;
        $dataOrder->save();
        $response=$dataOrder;
        $code=200; 
        // return apiResponseBuilder(200,$dataOrder);
        } catch (Exception $e) {
            if($e instanceof ValidationException){
                $response=$e->errors();
                $code=400;
            }else{
                $response=$e->getMessage();
                $code=500;
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {$data = OrderDetail::findOrFail($id);
           $code = 200;
           $response = $data;        
       } catch (Exception $e) {            
        if ($e instanceof ModelNotFoundException)
         {
               $code = 404;
               $response = "not found data";
           }else{
               $code = 500;
               $response = $e->getMessage();
           }
       }
       return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
