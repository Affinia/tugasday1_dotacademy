<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Customers;
use Exception;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try{
            $dataCustomers = customers::query();
            if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCustomers->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
            }
            $pagination = 5;
            $dataCustomers= $dataCustomers->orderBy('created_at','desc') //sorting
                                        ->paginate(5);

            // Handle perpindahan page
            $number = 0; // Default page

            if (request()->has('page') && request()->get('page') >= 1) {
                $number += (request()->get('page') - 1) * $pagination;
            }
            return apiResponseBuilder(200,$dataCustomers);
            }
            catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;  
            return apiResponseBuilder(500,$e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            
            ]);
            $dataCustomers = new Customers;

            $dataCustomers->first_name = $request->first_name;
            $dataCustomers->last_name = $request->last_name;
            $dataCustomers->email = $request->email;
            $dataCustomers->password = $request->password;
            $dataCustomers->address = $request->address;
            $dataCustomers->phone_number = $request->phone_number;

            $dataCustomers->save();
            $response=$dataCustomers;
            $code=200;  
        } catch (Exception $e) {
            if($e instanceof ValidationException){
                $response=$e->errors();
                $code=400;
            }else{
                $response=$e->getMessage();
                $code=500;
            }
                            
        }
        return apiResponseBuilder($code,$response); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            
            $dataCustomers = customers::find($id);
            return apiResponseBuilder(200,$dataCustomers);

        } catch (Exception $e) {
            if($e instanceof ModelNotFoundException){           
                $code=404;  
                return apiResponseBuilder(404,"Data Tidak Ada");
            }else{
                $response=$e->getMessage();
                $code=500;  
            return apiResponseBuilder(500,$e->getMessage());
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            
            ]);
            $dataCustomers = Customers::find($id);

            $dataCustomers->first_name = $request->first_name;
            $dataCustomers->last_name = $request->last_name;
            $dataCustomers->email = $request->email;
            $dataCustomers->password = $request->password;
            $dataCustomers->address = $request->address;
            $dataCustomers->phone_number = $request->phone_number;

            $dataCustomers->save();
            return apiResponseBuilder(200,$dataCustomers);
        } catch (Exception $e) {
            
            if($e instanceof ValidationException){
                $response=$e->errors();
                $code=400;
            }else{
                $response=$e->getMessage();
                $code=500;
            }
            return apiResponseBuilder($code,$response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dataCustomers = customers::find($id);

            $dataCustomers->delete();
            return apiResponseBuilder(200,$dataCustomers);
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;  
            return apiResponseBuilder(500,$e->getMessage());
    
        }
    }
}
