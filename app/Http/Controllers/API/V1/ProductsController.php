<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Products;
use App\Category;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use Exception;
use Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $response=ProductResource::collection(products::all());
            $code=200;
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
            'name' => 'required',
            'category_id' => 'required',
            'unit_price' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', //upload file
        ]);
            //query memnambah product_cout di category
            Category::where('id', $request->category_id)->increment('product_count');

            //upload file mengambil nama file
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            //menaruh di folder public/images 
            request()->image->move(public_path('images'), $imageName);
            //query product
            $dataProduct = new Products();
            $dataProduct->name = $request->name;
            $dataProduct->category_id = $request->category_id;
            $dataProduct->unit_price = $request->unit_price;
            $dataProduct->image = $imageName; //upload
            $dataProduct->save();
            
            // Session::flash('message','Data berhasil disimpan');
            $response=new ProductResource($dataProduct);
            $code=200;
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;
        }
            return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $response = new ProductResource(products::findOrFail($id));
            $code=200;
        } catch (Exception $e) {
            if($e instanceof ModelNotFoundException){           
                $code=404;  
                $response="DATA TIDAK ADA";
            }else{
                $response=$e->getMessage();
                $code=500;  
            
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
            'name' => 'required|min:3',
            'category_id' => 'required',
            'unit_price' => 'required|min:3|numeric',
        ]);
        $dataProduct = Products::find($id);

         if ($request->image) {

              // mengambil nama imagenya
            Storage::delete($request->images);
              $imageName = time().'.'.request()->image->getClientOriginalExtension();

              // menaruh image pada folder iages

              request()->image->move(public_path('/images'), $imageName);               $dataProduct->image       = $imageName;

          }
        $dataProduct->name = $request->name;
        $dataProduct->category_id = $request->category_id;
        $dataProduct->unit_price = $request->unit_price;

        $dataProduct->save();
        $response= new ProductResource($dataProduct);
        $code=200;
        
        } catch (Exception $e) {
            $response=$e->getMessage();
                $code=500;     
        }    
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dataProduct = Products::withTrashed()->find($id);

        if (!$dataProduct->trashed()) {

            $dataProduct->delete();

        }else{

            $dataProduct->forceDelete();

        }

            return apiResponseBuilder(200,$dataProduct);
        
        } catch (Exception $e) {
            $response=$e->getMessage();
            $code=500;  
            return apiResponseBuilder(500,$e->getMessage());
        }
    }
}
