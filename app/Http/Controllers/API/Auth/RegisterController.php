<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User; 
use Illuminate\Support\Facades\Auth; 

class RegisterController extends Controller
{
    public function register(Request $request)
    {
    	/*return response()->json(['test1'=> 'ok']);*/
   
  		$validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return apiResponseValidationFails('Validation error messages!', $validator->errors()->all());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $success['user'] = $user;
        $success['token'] = $user->createToken('myApp')->accessToken;

        return apiResponseSuccess('Register success!', $success, 200);
    	
    }
}
