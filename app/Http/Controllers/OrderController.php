<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Customers;
use App\Products;
use App\Orders;

class OrderController extends Controller
{
    public function index()
    {
        $dataCustomer = Customers::all();
        $dataOrder = Orders::all();

        return view('order.index', compact('dataCustomer', 'dataOrder'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'customer_id'   => 'required',
        ]);

        $dataOrder = new Orders;
        $dataOrder->customer_id = $request->customer_id;
        
        $dataOrder->total       = 0;
        $dataOrder->save();
    
        return redirect()->back()
                         ->with('success', 'Berhasil Tambah');
    }

    public function destroy($id)
    {
        // $dataOrder = Orders::find($id);
        // $dataOrder->delete();

        // return redirect()->back()
        //                  ->with('success', 'Berhasil Hapus');

        $dataOrder = orders::withTrashed()->find($id);

        if (!$dataOrder->trashed()) {

            $dataOrder->delete();

        }else{

            $dataOrder->forceDelete();

        }

        return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
    }

    public function detail($id)
    {
        $dataProduct = Products::all();
        $dataOrderDetail = OrderDetail::where('order_id', $id)->get();
        $detailId = $id;
        $total = Orders::select('total')->where('id', $id)->first();
        return view('order.detail', compact('dataProduct', 'detailId', 'dataOrderDetail', 'total'));
    }

    public function addProduct(Request $request, $id)
    {
        $request->validate([
            'product_id'    => 'required',
            'quantity'      => 'required',
        ]);

        // get data Order detail by product id
        $checkProduct = OrderDetail::where('product_id', $request->product_id)->where('order_id', $id)->first();
        //mengammbil data harga product
        $dataProduct= Products::where('id', $request->product_id)->first();        

        //cek jika produk sudah ada/belum
        if ($checkProduct) {            
            $checkProduct->order_id = $checkProduct->order_id;
            $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
            $checkProduct->price = $checkProduct->price + ($request->quantity * $dataProduct->unit_price);            
            $checkProduct->save();
        } else { //jika produk tidak ada maka menambahkan              
            $dataOrderDetail = new OrderDetail;
            $dataOrderDetail->order_id      = $id;
            $dataOrderDetail->product_id    = $request->product_id;
            $dataOrderDetail->quantity      = $request->quantity;
            $dataOrderDetail->price         = $request->quantity * $dataProduct->unit_price;
            $dataOrderDetail->save();
        }

        //ambil price  tabel simple order
        $getOrder = OrderDetail::where('order_id', $id)->get();
        //menjumlah semua price di order
        $total = $getOrder->sum('price');

        //insert data total order
        $dataOrder=Orders::find($id);
        $dataOrder->total=$total;
        $dataOrder->save();


        return redirect()->back()
                         ->with('success', 'Berhasil tambah produk');
    }

}
