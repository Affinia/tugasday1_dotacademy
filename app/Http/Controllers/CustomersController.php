<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use Session;
use Exception;

class CustomersController extends Controller
{
    public function index()
    {
    	$dataCustomers = Customers::query(); // Create empty query on Category table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCustomers->where(
                "first_name", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
         $dataCustomers= $dataCustomers->orderBy('created_at','desc') //sorting
                                    ->paginate(5);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        // $dataCategory = Categorys::all();
        return view('customers.index', compact('dataCustomers','number'));
    }

    public function create()
    {

        return view('customers.tambah');
    }
    public function detail($id)
    {
    	$dataCustomers = customers::find($id);
    	return view('customers.detail', compact('dataCustomers'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'first_name' => 'required|min:3',
    		'last_name' => 'required|min:3',
    		'email' => 'required|min:3',
    		'password' => 'required|min:3',
    		'address' => 'required|min:3',
    		'phone_number' => 'required|min:3|numeric',
    	]);

    	$dataCustomers = new Customers;

    	$dataCustomers->first_name = $request->first_name;
    	$dataCustomers->last_name = $request->last_name;
    	$dataCustomers->email = $request->email;
    	$dataCustomers->password = $request->password;
    	$dataCustomers->address = $request->address;
    	$dataCustomers->phone_number = $request->phone_number;

    	$dataCustomers->save();
    	if($dataCustomers)
    	{
    		Session::flash('message','Data berhasil disimpan');
    	}

    	return redirect()->back();
    }

     public function edit($id)
    {
    	$dataCustomers = customers::find($id);
    	return view('customers.edit', compact('dataCustomers'));
    }

    public function update(Request $request,$id)
    {
    	$this->validate($request,[
    		'first_name' => 'required|min:3',
    		'last_name' => 'required|min:3',
    		'email' => 'required|min:3',
    		'password' => 'required|min:3',
    		'address' => 'required|min:3',
    		'phone_number' => 'required|min:3|numeric',
    	]);

    	$dataCustomers = Customers::find($id);

    	$dataCustomers->first_name = $request->first_name;
    	$dataCustomers->last_name = $request->last_name;
    	$dataCustomers->email = $request->email;
    	$dataCustomers->password = $request->password;
    	$dataCustomers->address = $request->address;
    	$dataCustomers->phone_number = $request->phone_number;

    	$dataCustomers->save();
    	if($dataCustomers)
    	{
    		Session::flash('message','Data berhasil diupdate');
    	}

    	return redirect()->back();
    }

    public function delete($id)
    {
    	$dataCustomers = Customers::find($id);

    	$dataCustomers->delete();
    	if($dataCustomers)
    	{
    		Session::flash('message','Berhasih Delete Data');
    	}
    	return redirect()->back();
    }
}
