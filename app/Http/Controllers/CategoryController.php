<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Product;
use Session;
use Exception;

class CategoryController extends Controller
{
	public function index()
    {

        $dataCategory = Category::query(); // Create empty query on Category table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCategory->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
        }

    	// Query pagination
        $pagination = 5;
        $dataCategory= $dataCategory->orderBy('created_at','desc') //sorting
                                    ->paginate(5);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') >= 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

    	// $dataCategory = Categorys::all();
    	return view('Category.template', compact('dataCategory','number'));
    }

    

    public function detail($id)
    {
    	$dataCategory = Category::with("products")->find($id);
        return view("category.detail", compact('dataCategory'));

    }

    public function edit($id)
    {
    	$dataCategory = Category::find($id);
    	return view('Category.edit', compact('dataCategory'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|min:3',
    		
    	]);

    	$dataCategory = new Category;

    	$dataCategory->name = $request->name;
        $dataCategory->product_count = 0;
    	

    	$dataCategory->save();
    	if($dataCategory)
    	{
    		Session::flash('message','Data berhasil disimpan');
    	}

    	return redirect()->back();
    }

    public function update(Request $request,$id)
    {
    	$this->validate($request,[
    		'name' => 'required|min:3',

    		
    	]);
    	$dataCategory = Category::find($id);

    	$dataCategory->name = $request->name;
    	

    	$dataCategory->save();

    	if($dataCategory)
    	{
    		Session::flash('message','Berhasih Update Data');
    	}

    	return redirect()->back();
    }

    public function delete($id)
    {	

        
        $dataCategory = Category::withTrashed()->find($id);

        if (!$dataCategory->trashed()) {

            $dataCategory->delete();

        }else{

            $dataCategory->forceDelete();

        }

        return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
        
    }
}