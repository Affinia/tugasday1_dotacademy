<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Category;
use Exception;
use Storage;

use Session;

class ProductsController extends Controller
{
    public function index()
    {

        $dataProduct = Products::query(); // Create empty query on product table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataProduct->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
        }

    	// Query pagination
        $pagination = 5;
         $dataProduct= $dataProduct->orderBy('created_at','desc') //sorting 
                                
                                    ->paginate(5);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

    	// $dataProduct = Products::all();
        $dataCategory = Category::select(["id", "name"])->orderBy("name")->get();
    	return view('product.index', compact('dataProduct','number','dataCategory'));
    }

    

    public function detail($id)
    {
    	$dataProduct = Products::find($id);
    	return view('product.detail', compact('dataProduct'));
    }

    public function edit($id)
    {
        

    	$dataProduct = Products::find($id);
         $dataCategory = Category::select(["id", "name"])->orderBy("name")->get();
    	return view('product.edit', compact('dataProduct','dataCategory'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|min:3',
    		'category_id' => 'required',
    		'unit_price' => 'required|min:3|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', //upload file
    	]);

        try {
            DB::beginTransaction();
            //query memnambah product_cout di category
            Category::where('id', $request->category_id)->increment('product_count');

            //upload file mengambil nama file
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            //menaruh di folder public/images 
            request()->image->move(public_path('images'), $imageName);
            //query product
            $dataProduct = new Products;
            $dataProduct->name = $request->name;
            $dataProduct->category_id = $request->category_id;
            $dataProduct->unit_price = $request->unit_price;
            $dataProduct->image = $imageName; //upload
            $dataProduct->save();
            DB::commit();
            Session::flash('message','Data berhasil disimpan');
            return redirect()->back();

        } catch (Exception $e) {
            DB::rollBack();
            Session::flash('message','Data tidak berhasil disimpan');
            return redirect()->back();

        }
    }

    public function update(Request $request,$id)
    {
    	$this->validate($request,[
    		'name' => 'required|min:3',
    		'category_id' => 'required',
    		'unit_price' => 'required|min:3|numeric',
    	]);
    	$dataProduct = Products::find($id);

         if ($request->image) {

              // mengambil nama imagenya
            Storage::delete($request->images);
              $imageName = time().'.'.request()->image->getClientOriginalExtension();

              // menaruh image pada folder iages

              request()->image->move(public_path('/images'), $imageName);               $dataProduct->image       = $imageName;

          }
    	$dataProduct->name = $request->name;
    	$dataProduct->category_id = $request->category_id;
    	$dataProduct->unit_price = $request->unit_price;

    	$dataProduct->save();

    	if($dataProduct)
    	{
    		Session::flash('message','Berhasih Update Data');
    	}

    	return redirect()->back();
    }

    public function delete($id)
    {
    	
        $dataProduct = Products::withTrashed()->find($id);

        if (!$dataProduct->trashed()) {

            $dataProduct->delete();

        }else{

            $dataProduct->forceDelete();

        }

        return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
    }
}
