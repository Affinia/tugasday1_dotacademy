<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductResource;

use App\Category;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        "data"=> [
            "ïd" => $this->id,
            "name" => $this->name,
            "unit_price" => $this->unit_price,
            "image" => "http://localhost:2001/images/$this->image" ,
            "Category"=>[
                    "id" => $this->category_id,
                    "name" => $this->category->name
                ]
            ]
        ];
    }
}
