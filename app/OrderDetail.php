<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class OrderDetail extends Model
{
	use softdeletes;
    protected $table = 'order_details';

    protected $fillable = ['order_id', 'product_id', 'quantity', 'price'];

    public function Product() {
    	return $this->hasOne(Products::class, "id", "product_id");
    }

}
