<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use App\OrderDetail;
 

class Orders extends Model
{
	use softdeletes;
    protected $table = 'orders';

    protected $fillable = ['customer_id', 'total','order_id'];

    public function Customers() {
    	return $this->hasOne(Customers::class, "id", "customer_id");

    }

    public function OrderDetail()
    {
    	return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    }
    public function delete()
    {
        $this->OrderDetail()->delete();
        return parent::delete();
    }
}
