<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
     protected $table = 'products';

    protected $fillable = ["name", "category_id", "unit_price"];

    use softDeletes;
    public function category() 
    {
    	return $this->hasOne(Category::class, "id", "category_id")->withTrashed();
    }

    
}
